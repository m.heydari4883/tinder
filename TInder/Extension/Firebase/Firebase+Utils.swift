//
//  Firebase+Utils.swift
//  TInder
//
//  Created by Masoud Heydari on 8/5/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import Firebase

extension Firestore {
    func fetchCurrentUser(completion: @escaping (User?, Error?) -> ()) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let db = Firestore.firestore()
        db.collection("users")
            .document(uid)
            .getDocument { (snapshot, err) in
                if let err = err {
                    completion(nil, err)
                    return
                }
                
                // fetched our user here
                guard let dictionary = snapshot?.data() else {
                    let error = NSError(domain: "com.lbta.swipematch", code: 500, userInfo: [NSLocalizedDescriptionKey: "No user found in Firestore"])
                    completion(nil, error)
                    return
                }
                let user = User(dictionary: dictionary)
                completion(user, nil)
        }
    }
}
