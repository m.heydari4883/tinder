//
//  SettingsController.swift
//  TInder
//
//  Created by Masoud Heydari on 8/1/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit
import Firebase
import JGProgressHUD
import SDWebImage


protocol SettingsControllerDelegate: class {
    func didSaveSettings()
}

class SettingsController: UITableViewController {
    
    weak var delegate: SettingsControllerDelegate?
    let settingsHeaderView = SettingsHeaderView()
    let imagePickerController = CustomImagePickerController()
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationItems()
        setupHeaderView()
        setupTableView()
        fetchCurrentUser()
    }
    
    private func setupTableView() {
        tableView.backgroundColor = .init(white: 0.95, alpha: 1)
        tableView.tableFooterView = UIView()
        tableView.keyboardDismissMode = .interactive
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    private func setupHeaderView() {
        settingsHeaderView.handleBtnSelectPhotoTapped = { [weak self] (imageButton) in
            guard let strongSelf = self else { return }
            strongSelf.present(strongSelf.imagePickerController, animated: true)
            strongSelf.imagePickerController.delegate = self
            strongSelf.imagePickerController.imageButton = imageButton
        }
    }
    
    private func setupNavigationItems() {
        navigationItem.title = "Settings"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        let cancelBurButton = createBarButtonItem(title: "Cancel", selector: #selector(handleBtnCancelTapped))
        let saveBarButton = createBarButtonItem(title: "Save", selector: #selector(handleBtnSaveTapped))
        let logOutBarButton = createBarButtonItem(title: "Logout", selector: #selector(handleBtnLogoutTapped))
        
        navigationItem.leftBarButtonItem = cancelBurButton
        navigationItem.rightBarButtonItems = [saveBarButton, logOutBarButton]
        
    }
    
    private func fetchCurrentUser() {
        Firestore.firestore().fetchCurrentUser { (user, err) in
            if let err = err {
                print("Failed to fetch user:", err)
                return
            }
            self.user = user
            self.loadUserPhotos()
            self.tableView.reloadData()
        }
    }
    
    fileprivate func loadUserPhotos() {
        if let imageUrl = user?.imageUrl1, let url = URL(string: imageUrl) {
            SDWebImageManager.shared().loadImage(with: url, options: .continueInBackground, progress: nil) { (image, _, _, _, _, _) in
                self.settingsHeaderView.imgBtn1.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
            }
        }
        if let imageUrl = user?.imageUrl2, let url = URL(string: imageUrl) {
            SDWebImageManager.shared().loadImage(with: url, options: .continueInBackground, progress: nil) { (image, _, _, _, _, _) in
                self.settingsHeaderView.imgBtn2.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
            }
        }
        if let imageUrl = user?.imageUrl3, let url = URL(string: imageUrl) {
            SDWebImageManager.shared().loadImage(with: url, options: .continueInBackground, progress: nil) { (image, _, _, _, _, _) in
                self.settingsHeaderView.imgBtn3.setImage(image?.withRenderingMode(.alwaysOriginal), for: .normal)
            }
        }
    }
    
    private func createBarButtonItem(title: String, selector: Selector) -> UIBarButtonItem {
        return UIBarButtonItem(title: title, style: .plain, target: self, action: selector)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // age range cell
        if indexPath.section == 5 {
            let ageRangeCell = AgeRangeCell(style: .default, reuseIdentifier: nil)
            
            let minValue = Float(user?.minSeekingAge ?? 0)
            let maxValue = Float(user?.maxSeekingAge ?? 0)
            
            ageRangeCell.minSlider.value = minValue
            ageRangeCell.maxSlider.value = maxValue
            
            ageRangeCell.minLabel.text = "Min \(Int(minValue))"
            ageRangeCell.maxLabel.text = "Max \(Int(maxValue))"
            
            ageRangeCell.minSlider.addTarget(self, action: #selector(handleMinAgeChange), for: .valueChanged)
            ageRangeCell.maxSlider.addTarget(self, action: #selector(handleMaxAgeChange), for: .valueChanged)
            
            return ageRangeCell
        }
        
        let cell = SettingsCell(style: .default, reuseIdentifier: nil)
        
        switch indexPath.section {
        case 1:
            cell.textField.placeholder = "Enter Name"
            cell.textField.text = user?.name
            cell.textField.addTarget(self, action: #selector(handleNameChange), for: .editingChanged)
        case 2:
            cell.textField.placeholder = "Enter Profession"
            cell.textField.text = user?.profession
            cell.textField.addTarget(self, action: #selector(handleProfessionChange), for: .editingChanged)
        case 3:
            cell.textField.placeholder = "Enter Age"
            cell.textField.addTarget(self, action: #selector(handleAgeChange), for: .editingChanged)
            if let age = user?.age {
                cell.textField.text = String(age)
            }
        case 4:
            cell.textField.placeholder = "Enter Bio"
        default:
            ()
        }
        
        return cell
    }
    
    @objc fileprivate func handleNameChange(textField: UITextField) {
        self.user?.name = textField.text
    }
    
    @objc fileprivate func handleProfessionChange(textField: UITextField) {
        self.user?.profession = textField.text
    }
    
    @objc fileprivate func handleAgeChange(textField: UITextField) {
        self.user?.age = Int(textField.text ?? "")
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 300
        }
        
        return 50
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 0 : 1
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerLabel = HeaderLabel()
        headerLabel.font = UIFont.boldSystemFont(ofSize: 16)
        
        switch section {
        case 0:
            return settingsHeaderView
        case 1:
            headerLabel.text = "Name"
        case 2:
            headerLabel.text = "Profession"
        case 3:
            headerLabel.text = "Age"
        case 4:
            headerLabel.text = "Bio"
        case 5:
            headerLabel.text = "Seeking Age Range"
        default:
            ()
        }
        
        return headerLabel
    }
    
    private func evaluateMinMax() {
        guard let ageRangeCell = tableView.cellForRow(at: [5, 0]) as? AgeRangeCell else { return }
        let minValue = Int(ageRangeCell.minSlider.value)
        var maxValue = Int(ageRangeCell.maxSlider.value)
        maxValue = max(minValue, maxValue)
        ageRangeCell.maxSlider.value = Float(maxValue)
        ageRangeCell.minLabel.text = "Min \(minValue)"
        ageRangeCell.maxLabel.text = "Max \(maxValue)"
        
        user?.minSeekingAge = minValue
        user?.maxSeekingAge = maxValue
    }
    
    @objc private func handleMinAgeChange(slider: UISlider) {
        evaluateMinMax()
    }
    
    @objc private func handleMaxAgeChange(slider: UISlider) {
        evaluateMinMax()
    }
    
    @objc private func handleBtnLogoutTapped() {
        try? Auth.auth().signOut()
        dismiss(animated: true)
    }
    
    @objc private func handleBtnSaveTapped() {
        print("Saving our settings data into Firestore")
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let docData: [String: Any] = [
            "uid": uid,
            "fullName": user?.name ?? "",
            "imageUrl1": user?.imageUrl1 ?? "",
            "imageUrl2": user?.imageUrl2 ?? "",
            "imageUrl3": user?.imageUrl3 ?? "",
            "age": user?.age ?? -1,
            "profession": user?.profession ?? "",
            "minSeekingAge": user?.minSeekingAge ?? -1,
            "maxSeekingAge": user?.maxSeekingAge ?? -1
        ]
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Saving settings"
        hud.show(in: view)
        Firestore.firestore().collection("users").document(uid).setData(docData) { (err) in
            hud.dismiss()
            if let err = err {
                print("Failed to save user settings:", err)
                return
            }
            
            print("Finished saving user info")
            self.dismiss(animated: true, completion: {
                print("Dismissal complete")
                self.delegate?.didSaveSettings()
            })
        }
    }
    
    @objc private func handleBtnCancelTapped() {
        dismiss(animated: true)
    }
    
}

extension SettingsController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let selectedImage = info[.originalImage] as? UIImage
        guard let imageButton = (picker as? CustomImagePickerController)?.imageButton else { return }
        imageButton.setImage(selectedImage?.withRenderingMode(.alwaysOriginal), for: .normal)
        dismiss(animated: true)
        
        
        let filename = UUID().uuidString
        let reference = Storage.storage().reference(withPath: "/images/\(filename)")
        guard let uploadData = selectedImage?.jpegData(compressionQuality: 0.75) else { return }
        
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Uploading image..."
        hud.show(in: view)
        reference.putData(uploadData, metadata: nil) { (nil, err) in
            if let err = err {
                hud.dismiss()
                print("Failed to upload image to storage:", err)
                return
            }
            
            print("Finished uploading image")
            reference.downloadURL(completion: { (url, err) in
                
                hud.dismiss()
                
                if let err = err {
                    print("Failed to retrieve download URL:", err)
                    return
                }
                
                print("Finished getting download url:", url?.absoluteString ?? "")
                
                if imageButton == self.settingsHeaderView.imgBtn1 {
                    self.user?.imageUrl1 = url?.absoluteString
                } else if imageButton == self.settingsHeaderView.imgBtn2 {
                    self.user?.imageUrl2 = url?.absoluteString
                } else {
                    self.user?.imageUrl3 = url?.absoluteString
                }
            })
        }
        
    }
}
