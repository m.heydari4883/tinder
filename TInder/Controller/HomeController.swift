//
//  ViewController.swift
//  TInder
//
//  Created by Masoud Heydari on 7/26/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit
import JGProgressHUD
import Firebase

class HomeController: UIViewController {
    
    let topStackView = HomeTopNavigationStackView()
    let cardsDeckView = UIView()
    let buttonsStackView = HomeButtonControlsStackView()
    
    let overalStackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    var cardViewModels = [CardViewModel]()
    var lastFetchedUser: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        //        setupFirestoreUserCards()
        fetchUsersFromFirestore()
    }
    
    private func setupView() {
        view.backgroundColor = .white
        setupOverallStackView()
        
        topStackView.btnSettings.addTarget(self, action: #selector(handleBtnSettingsTapped), for: .touchUpInside)
        buttonsStackView.btnRefresh.addTarget(self, action: #selector(handleBtnRefreshTapped), for: .touchUpInside)
    }
    
    private func fetchUsersFromFirestore() {
        let hud = JGProgressHUD(style: .dark)        // view
        hud.textLabel.text = "Fetching Users"        // view
        hud.show(in: view)        // view
        
        let query = Firestore.firestore().collection("users").order(by: "uid").start(at: [lastFetchedUser?.uid ?? ""]).limit(to: 2)
        query.getDocuments { (snapShot, error) in
            hud.dismiss() // view
            self.performCleanupForRefresh() // view
            if let error = error {
                print("Failed to fetch users! ", error.localizedDescription)
                return
            }
            
            snapShot?.documents.forEach({ (documentSnapShot) in
                let userDictionary = documentSnapShot.data()
                let user = User(dictionary: userDictionary)
                self.cardViewModels.append(user.toCardViewModel())
                self.lastFetchedUser = user
                self.setupCardFromUser(user: user)
            })
        }
    }
    
    private func setupCardFromUser(user: User) {
        print("user name \(user.name ?? "")")
        let cardView = CardView(frame: .zero)
        cardView.cardViewModel = user.toCardViewModel()
        cardsDeckView.addSubview(cardView)
        cardsDeckView.sendSubviewToBack(cardView)
        cardView.fillToSuperview()
    }
    
    private func setupFirestoreUserCards() {
        cardViewModels.forEach { (cardViewModel) in
            let cardView = CardView(frame: .zero)
            cardView.cardViewModel = cardViewModel
            cardsDeckView.addSubview(cardView)
            cardView.fillToSuperview()
        }
    }
    
    private func performCleanupForRefresh() {
        cardViewModels.removeAll()
        self.cardsDeckView.subviews.forEach { $0.removeFromSuperview() }
    }
    
    private func setupOverallStackView() {
        [topStackView, cardsDeckView, buttonsStackView].forEach{ overalStackView.addArrangedSubview($0)}
        overalStackView.axis = .vertical
        view.addSubview(overalStackView)
        let safeArea = view.safeAreaLayoutGuide
        [overalStackView.leftAnchor.constraint(equalTo: safeArea.leftAnchor),
         overalStackView.rightAnchor.constraint(equalTo: safeArea.rightAnchor),
         overalStackView.topAnchor.constraint(equalTo: safeArea.topAnchor),
         overalStackView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor)
            ].forEach { $0.isActive = true }
        
        overalStackView.isLayoutMarginsRelativeArrangement = true
        overalStackView.layoutMargins = Size.Controller.Home.overallStackViewMargin
        overalStackView.bringSubviewToFront(cardsDeckView)
    }
    
    @objc private func handleBtnRefreshTapped(_ sender: UIButton) {
        lastFetchedUser = nil
        fetchUsersFromFirestore()
    }
    
    @objc private func handleBtnSettingsTapped(_ sender: UIButton) {
        let settingController = SettingsController()
        let navSettingsController = UINavigationController(rootViewController: settingController)
        present(navSettingsController, animated: true)
    }
}

