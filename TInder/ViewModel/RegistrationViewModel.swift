//
//  RegistrationViewModel.swift
//  TInder
//
//  Created by Masoud Heydari on 7/28/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit
import Firebase

class RegistrationViewModel {
    
    var fullName: String? {
        didSet {
            checkFormValidity()
        }
    }
    
    var email: String? {
        didSet {
            checkFormValidity()
        }
    }
    
    var password: String? {
        didSet {
            checkFormValidity()
        }
    }
    
    var textFieldsAndButtonCount: Int? {
        didSet {
            if textFieldsAndButtonCount == 5 {
                bindableCanHideSpacerView.value = true
            }
        }
    }
    
    private func checkFormValidity() {
        let isFormValid = fullName?.isEmpty == false && email?.isEmpty == false && password?.isEmpty == false
        self.bindableIsFormValid.value = isFormValid
    }
    
    func performRegistration(completion: @escaping (Error?) -> ()) {
        guard let email = email, let password = password else { return }
        bindableIsRegistering.value = true
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let error = error {
                // there is some error
                completion(error)
            }
            
            print("User Successfully Registered! ", result?.user.uid ?? "")
            self.saveImageToFireStore(completion: completion)
        }
    }
    
    private func saveImageToFireStore(completion: @escaping (Error?) -> ()) {
        let fileName = UUID().uuidString
        let reference = Storage.storage().reference(withPath: "/images/\(fileName)")
        guard let imageData = self.bindableImage.value?.jpegData(compressionQuality: 0.75) else { return }
        reference.putData(imageData, metadata: nil) { (_, error) in
            if let error = error {
                completion(error)
                return
            }
            
            print("user image profile successfully uploaded!")
            reference.downloadURL(completion: { (url, error) in
                
                let imageUrl = url?.absoluteString ?? Const.empty
                self.saveInfoToFirestore(imageUrl: imageUrl, completion: completion)
            })
        }
    }
    
    private func saveInfoToFirestore(imageUrl: String, completion: @escaping (Error?) -> ()) {
        guard let fullName = fullName else { return }
        let uid = Auth.auth().currentUser?.uid ?? Const.empty
        let docData = ["fullName" : fullName, "uid" : uid, "imageUrl1" : imageUrl]
        
        Firestore.firestore().collection("users").document(uid).setData(docData) { (error) in
            self.bindableIsRegistering.value = false
            if let error = error {
                print("error is \(error.localizedDescription)")
                completion(error)
                return
            }
            
            completion(nil)
        }
    }
    
    var bindableIsFormValid = Bindable<Bool>()
    var bindableImage = Bindable<UIImage>()
    var bindableCanHideSpacerView = Bindable<Bool>()
    var bindableIsRegistering = Bindable<Bool>()
}
