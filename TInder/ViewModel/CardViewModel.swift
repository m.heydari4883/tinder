//
// Created by Masoud Heydari on 2019-07-28.
// Copyright (c) 2019 Masoud Heydari. All rights reserved.
//

import UIKit

protocol ProducesCardViewModel {
    func toCardViewModel() -> CardViewModel
}

class CardViewModel {
    
    let imageNames: [String]
    let attributedString: NSAttributedString
    let textAlignment: NSTextAlignment
    
    var imageIndexObserver: ((Int, String?) -> ())?
    
    private var imageIndex: Int = 0 {
        didSet {
            let imageUrl = imageNames[imageIndex]
            print("image url \(imageUrl) || image count \(imageNames.count) || img index \(imageIndex)")
            imageIndexObserver?(imageIndex, imageUrl)
        }
    }
    
    init(imageNames: [String], attributedString: NSAttributedString, textAlignment: NSTextAlignment) {
        self.imageNames = imageNames
        self.attributedString = attributedString
        self.textAlignment = textAlignment
    }
    
    func goToNextPhoto() {
        imageIndex = min(imageIndex + 1, imageNames.count - 1)
    }
    
    func goToPreviousPhoto() {
        imageIndex = max(0, imageIndex - 1)
    }
    
    func shouldAdvanceNextPhoto(point: CGFloat, frameWidth: CGFloat) {
        let shouldAdvanceNextPhoto = point > frameWidth / 2 ? true : false
        
        if shouldAdvanceNextPhoto {
            goToNextPhoto()
        } else {
            goToPreviousPhoto()
        }
    }
    
}
