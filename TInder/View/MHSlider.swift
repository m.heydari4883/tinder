//
//  MHSlider.swift
//  TInder
//
//  Created by Masoud Heydari on 8/2/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class MHSlider: UISlider {
    
    init(minValue: Float, maxValue: Float) {
        super.init(frame: .zero)
        self.minimumValue = minValue
        self.maximumValue = maxValue
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
