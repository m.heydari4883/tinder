//
//  SettingsCell.swift
//  TInder
//
//  Created by Masoud Heydari on 8/1/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {
    
    let textField: CustomTextField = {
        let textField = CustomTextField(padding: 16, height: 0)
        return textField
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        addSubview(textField)
        textField.fillToSuperview()
    }
}

