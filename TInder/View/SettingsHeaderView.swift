//
//  SettingsHeaderView.swift
//  TInder
//
//  Created by Masoud Heydari on 8/1/19.
//  Copyright © 2019 Masoud Heydari. All rights reserved.
//

import UIKit

class SettingsHeaderView: UIView {
    
    var handleBtnSelectPhotoTapped: ((UIButton) -> ())?

    let imgBtn1: UIButton = {
        let btn = UIButton(type: .system)
        btn.layer.cornerRadius = 8
        btn.backgroundColor = .white
        btn.clipsToBounds = true
        btn.imageView?.contentMode = .scaleAspectFill
        btn.setTitle("Select Photo", for: .normal)
        btn.setTitleColor(UIColor(white: 0.8, alpha: 1), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        btn.addTarget(self, action: #selector(handleImagButtonTapped), for: .touchUpInside)
        return btn
    }()
    
    let imgBtn2: UIButton = {
        let btn = UIButton(type: .system)
        btn.layer.cornerRadius = 8
        btn.backgroundColor = .white
        btn.clipsToBounds = true
        btn.imageView?.contentMode = .scaleAspectFill
        btn.setTitle("Select Photo", for: .normal)
        btn.setTitleColor(UIColor(white: 0.8, alpha: 1), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        btn.addTarget(self, action: #selector(handleImagButtonTapped), for: .touchUpInside)
        return btn
    }()
    
    let imgBtn3: UIButton = {
        let btn = UIButton(type: .system)
        btn.layer.cornerRadius = 8
        btn.backgroundColor = .white
        btn.clipsToBounds = true
        btn.imageView?.contentMode = .scaleAspectFill
        btn.setTitle("Select Photo", for: .normal)
        btn.setTitleColor(UIColor(white: 0.8, alpha: 1), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .heavy)
        btn.addTarget(self, action: #selector(handleImagButtonTapped), for: .touchUpInside)
        return btn
    }()
    
    let overallStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    let verticalStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .fillEqually
        verticalStackView.spacing = 16
        
        verticalStackView.addArrangedSubview(views: [imgBtn2, imgBtn3])
        
        overallStackView.distribution = .fillEqually
        overallStackView.spacing = 16
        overallStackView.addArrangedSubview(views: [imgBtn1, verticalStackView])
        
        addSubview(overallStackView)
        overallStackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        overallStackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        overallStackView.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        overallStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16).isActive = true
    }
    
    @objc private func handleImagButtonTapped(_ sender: UIButton) {
        handleBtnSelectPhotoTapped?(sender)
    }

}
